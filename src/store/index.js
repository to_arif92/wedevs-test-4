import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import ProductBrand from '../model/ProductBrand'
import Product from "../model/Product";
import ProductCategory from "../model/ProductCategory";
import VariantCategory from "../model/VariantCategory";

export default new Vuex.Store({
    state: {
        product: new Product(),
        productCategory: new ProductCategory(),
        productBrand: new ProductBrand(),
        variantCategory: new VariantCategory(),
        activeFormData: {},
        activeFormType: '',
        activeOperation: 'read',
        activeItemIndex: 0,
        activeFormRouterName: '',
        modalFlag: false,
        nestedModalFlag: false,
        allCategory: JSON.parse(localStorage.getItem('categories')) || [],
        allVariant: JSON.parse(localStorage.getItem('variants')) || [],
        allProductBrand: JSON.parse(localStorage.getItem('brands')) || [],
        allProduct: JSON.parse(localStorage.getItem('products')) || [],
    },
    getters: {
        getAllCategories: (state) => state.allCategory,
        getEditCategory: (state) => state.allCategory[state.activeItemIndex],

        getAllVariants: (state) => state.allVariant,
        getEditVariant: (state) => state.allVariant[state.activeItemIndex],

        getAllProductBrands: (state) => state.allProductBrand,
        getEditProductBrand: (state) => state.allProductBrand[state.activeItemIndex],

        getAllProducts: (state) => state.allProduct,
        getEditProduct: (state) => state.allProduct[state.activeItemIndex],
    },
    mutations: {
        closeModal (state, nestedFlag) {
            state.nestedModalFlag = nestedFlag
            nestedFlag ? state.nestedModalFlag = false : state.modalFlag = true
            !nestedFlag && state.modalFlag ? state.modalFlag = false : state.modalFlag = true
        },
        openModal (state, nestedFlag) {
            state.nestedModalFlag = nestedFlag
            !nestedFlag ? state.modalFlag = true : (state.modalFlag ? state.modalFlag = true : state.nestedModalFlag = true)
        },
        routeCrud (state, type) {
            let buildRouterName = 'home'

            /* if modal closed */
            if (!state.modalFlag) {
                buildRouterName = state.activeFormType
            }
            /* when modal opened from list view */
            else {
                if (state.nestedModalFlag || (!state.nestedModalFlag && state.activeOperation === 'read')) {
                    buildRouterName = state.activeFormType
                }
                else {
                    if (state.activeOperation === undefined) buildRouterName = type+'_create'
                    else buildRouterName = type+'_'+state.activeOperation
                }
            }
            state.activeFormRouterName = buildRouterName
        },

        saveProductCategory (state, value) {
            let obj = Object.assign({}, value);
            state.allCategory.push(obj)
            localStorage.setItem('categories', JSON.stringify(state.allCategory))
        },
        updateProductCategory (state, value) {
            state.productCategory = value
            state.allCategory[state.activeItemIndex] = value
            localStorage.setItem('categories', JSON.stringify(state.allCategory))
        },
        deleteProductCategory (state, idx) {
            state.allCategory.splice(idx, 1)
            localStorage.setItem('categories', JSON.stringify(state.allCategory))
        },

        saveProductBrand (state, value) {
            let obj = Object.assign({}, value);
            state.allProductBrand.push(obj)
            localStorage.setItem('brands', JSON.stringify(state.allProductBrand))
        },
        updateProductBrand (state, value) {
            state.productProductBrand = value
            state.allProductBrand[state.activeItemIndex] = value
            localStorage.setItem('brands', JSON.stringify(state.allProductBrand))
        },
        deleteProductBrand (state, idx) {
            state.allProductBrand.splice(idx, 1)
            localStorage.setItem('brands', JSON.stringify(state.allProductBrand))
        },

        saveProduct (state, value) {
            let obj = Object.assign({}, value);
            state.allProduct.push(obj)
            localStorage.setItem('products', JSON.stringify(state.allProduct))
        },
        updateProduct (state, value) {
            state.product = value
            state.allProduct[state.activeItemIndex] = value
            localStorage.setItem('products', JSON.stringify(state.allProduct))
        },
        deleteProduct (state, idx) {
            state.allProduct.splice(idx, 1)
            localStorage.setItem('products', JSON.stringify(state.allProduct))
        },

        saveVariantCategory (state, value) {
            let obj = Object.assign({}, value);
            state.allVariant.push(obj)
            localStorage.setItem('variants', JSON.stringify(state.allVariant))
        },
        updateVariantCategory (state, value) {
            state.variantCategory = value
            state.allVariant[state.activeItemIndex] = value
            localStorage.setItem('variants', JSON.stringify(state.allVariant))
        },
        deleteVariantCategory (state, idx) {
            state.allVariant.splice(idx, 1)
            localStorage.setItem('variants', JSON.stringify(state.allVariant))
        },

        setActiveFormType (state, value) {
            state.activeFormType = value
        },
        setActiveFormData (state, value) {
            Object.assign(state.activeFormData, value)
        },
        setActiveOperation (state, value) {
            state.activeOperation = value
        },
        setActiveItemIndex (state, value) {
            state.activeItemIndex = value
        }
    },
    actions: {
        openModalAction (ctx, value) {
            /* decide if should open in nested */
            let isNested = false
            if (value.isNested !== undefined) isNested = true
            /* set active operation */
            let currentOperation = ctx.state.activeOperation
            if (value.ops === undefined) currentOperation = 'create'
            else if (value.ops === 'update') currentOperation = 'update'
            else if (value.ops === 'delete') currentOperation = 'delete'
            ctx.commit('setActiveOperation', currentOperation)

            if (value.itemIndex !== undefined)
            ctx.commit('setActiveItemIndex', value.itemIndex)

            ctx.commit('openModal', isNested)
            ctx.commit('routeCrud', ctx.state.activeFormType)
        },
        closeModalAction ({state, commit}, isNested) {
            commit('setActiveOperation', 'read')
            commit('closeModal', isNested === true)
            commit('routeCrud', state.activeFormType)
        },
        syncModelAction ({state}, value) {
            state.activeFormData = value
        },
        createFormDataAction (ctx, value) {
            value = ctx.state.activeFormData
            switch (ctx.state.activeFormType) {
                case 'product_category':
                    ctx.commit('saveProductCategory', value)
                    break;
                case 'variant_category':
                    ctx.commit('saveVariantCategory', value)
                    break;
                case 'product_brand':
                    ctx.commit('saveProductBrand', value)
                    break;
                case 'product':
                    ctx.commit('saveProduct', value)
                    break;
            }
            ctx.dispatch('closeModalAction')
        },
        updateFormDataAction (ctx, value) {
            ctx.commit('setActiveFormData', value)
            switch (ctx.state.activeFormType) {
                case 'product_category':
                    ctx.commit('updateProductCategory', value)
                    break;
                case 'variant_category':
                    ctx.commit('updateVariantCategory', value)
                    break;
                case 'product_brand':
                    ctx.commit('updateProductBrand', value)
                    break;
                case 'product':
                    ctx.commit('updateProduct', value)
                    break;
            }
            ctx.dispatch('closeModalAction')
        },
        deleteFormDataAction (ctx, idx) {
            ctx.commit('setActiveFormData', idx)
            switch (ctx.state.activeFormType) {
                case 'product_category':
                    ctx.commit('deleteProductCategory', idx)
                    break;
                case 'variant_category':
                    ctx.commit('deleteVariantCategory', idx)
                    break;
                case 'product_brand':
                    ctx.commit('deleteProductBrand', idx)
                    break;
                case 'product':
                    ctx.commit('deleteProduct', idx)
                    break;
            }
        },
        setActiveFormTypeAction({commit, state}, value) {
            if (!state.nestedModalFlag) // comment out for router view
            commit('setActiveFormType', value)
        },
        setActiveOperationAction({commit}, value) {
            commit('setActiveOperation', value)
        }
    }
})