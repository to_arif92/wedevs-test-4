import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store/index'
import 'bulma/css/bulma.min.css'
import MultiSelect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'

Vue.component('multi-select', MultiSelect)
import BaseFormActions from "./components/core/BaseFormActions";
Vue.component('BaseFormActions', BaseFormActions)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
