export default class {
    constructor(name, brand, isStockable, description, sku, categories, usp, variantCategories) {
        this.name = name;
        this.brand = brand;
        this.isStockable = isStockable;
        this.description = description;
        this.sku = sku;
        this.categories = categories;
        this.usp = usp;
        this.variantCategories = variantCategories;
    }
}