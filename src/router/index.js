import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Home from "../components/Home";
import ProductBase from "../components/ProductBase"
import ProductCategoryBase from "../components/ProductCategoryBase";
import ProductBrandBase from "../components/ProductBrandBase";
import VariantCategoryBase from "../components/VariantCategoryBase";

export default new VueRouter({
    routes: [
        {
            path: '*',
            component: Home,
            name: 'NotFound'
        },
        {
            path: '/',
            component: Home,
            name: 'home'
        },
        {
            path: '/product',
            component: ProductBase,
            name: 'product',
            children: [
                {
                    path: '/product/create',
                    component: () => import('../components/product/AddProduct'),
                    name: 'product_create'
                },
                {
                    path: '/product/update',
                    component: () => import('../components/product/UpdateProduct'),
                    name: 'product_update'
                },
                {
                    path: '/product/delete',
                    component: () => import('../components/product/AddProduct'),
                    name: 'product_delete'
                }
            ]
        },
        {
            path: '/product-category',
            component: ProductCategoryBase,
            name: 'product_category',
            children: [
                {
                    path: '/product-category/create',
                    component: () => import('../components/category/AddProductCategory'),
                    name: 'product_category_create'
                },
                {
                    path: '/product-category/update',
                    component: () => import('../components/category/UpdateProductCategory'),
                    name: 'product_category_update'
                },
                {
                    path: '/product-category/delete',
                    component: () => import('../components/category/AddProductCategory'),
                    name: 'product_category_delete'
                }
            ]
        },
        {
            path: '/product-brand',
            component: ProductBrandBase,
            name: 'product_brand',
            children: [
                {
                    path: '/product-brand/create',
                    component: () => import('../components/product-brand/AddProductBrand'),
                    name: 'product_brand_create'
                },
                {
                    path: '/product-brand/update',
                    component: () => import('../components/product-brand/UpdateProductBrand'),
                    name: 'product_brand_update'
                },
                {
                    path: '/product-brand/delete',
                    component: () => import('../components/product-brand/AddProductBrand'),
                    name: 'product_brand_delete'
                },
            ]
        },
        {
            path: '/variant-category',
            component: VariantCategoryBase,
            name: 'variant_category',
            children: [
                {
                    path: '/variant-category/create',
                    component: () => import('../components/variant-category/AddVariantCategory'),
                    name: 'variant_category_create'
                },
                {
                    path: '/variant-category/update',
                    component: () => import('../components/variant-category/UpdateVariantCategory'),
                    name: 'variant_category_update'
                },
                {
                    path: '/variant-category/delete',
                    component: () => import('../components/variant-category/AddVariantCategory'),
                    name: 'variant_category_delete'
                },
            ]
        }
    ]
})