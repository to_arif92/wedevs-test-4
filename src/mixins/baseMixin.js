import {mapActions, mapState} from "vuex";

export default {
    watch: {
        activeFormRouterName(n) {
            if (!this.nestedModalFlag)  // comment out for routed nested form
            this.$router.push({name: n})
        }
    },
    methods: {
        ...mapActions(['setActiveFormTypeAction']),
    },
    computed: {
        ...mapState(['activeFormRouterName', 'nestedModalFlag']),
    },
    mounted() {
        this.setActiveFormTypeAction(this.typeName)
    }
}