import {mapActions, mapState} from "vuex";

export default {
    computed: {
        ...mapState(['activeOperation']),
        getPreTitle() {
            return this.activeOperation === 'update'? 'Edit' : 'Add new'
        }
    },
    methods: {
        ...mapActions(['updateFormDataAction', 'syncModelAction']),
        syncModel(m){
            let ops = this.activeOperation;
            if (ops === 'create') this.syncModelAction(m)
            else if (ops === 'update') this.syncModelAction(m)
        }
    }
}