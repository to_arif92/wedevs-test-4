import {mapActions, mapState} from "vuex";

export default {
    computed: {
        ...mapState(['modalFlag', 'nestedModalFlag'])
    },
    methods: {
        ...mapActions(['openModalAction', 'closeModalAction'])
    }
}